# Questions

### Tell me about yourself.

### How would others describe you?

### What is your greatest strength?

### What is your greatest weakness?

### Describe your most significant accomplishment.

### What programming languages do you prefer and why?

### Describe the difference between C and C++?

# Terms

### Dynamic Programming
- Method for solving large problems by breaking them into small parts that can be solved once and reused.

### Programming Language
- Tokens and gramatical rules that can be used to compile machine code.

### Scripting Language
- Tokens and gramatical rules that can be executed by an interpreter.

### Compiler
- Translates computer code from one language to another.

### Cross Compiler
- Compiling code for a different architecture than the one being compiled on.

### Interpreter
- Runs a langauge directly without needing to translate it to machine code.

### Segfault
- A crash that occurs when accessing illegal memory locations.

### void
- Can mean no return value, no parameters, or universal pointer.

### const
- Prevents modification of variable with this modifier.

### Encapsulation
- Defines what should have access to members of a class.
- Object Oriented Programming

### public
- Any class can access a member with this modifier.
- Object Oriented Programming

### protected
- Only the owning class and its derivatives can access a member with this modifier.
- Object Oriented Programming

### private
- Only the owning class can access a member with this modifier.
- Object Oriented Programming

### static
- Variables with this modifier have a scope of the entire duration of the program.
- In C, a global variable marked as static means its scope is limited to that file.

### Lock
- Used to protect a critical section, only one thread in a process can aquire, and then must release.
- Other threads are put to sleep until the lock is released.

### Mutex / Binary Semephore
- Used to allow only one thread across processes to execute a critical section.

### Semephore / Counting Semephore
- Used to allow a defined max number of threads to aquire, execute, and release.
- When the max is reached further threads are put to sleep until other threads release.

### Parallelism
- Program splits tasks so they can be run across multiple cpus at the same time.

### Concurrent Programming
- Programming paradigm divides time across multiple tasks.

### Busy Wait
- Continuously polling for an event before continuing.

### Critical Section
- Shared resource that is accessed or modified by multiple threads.

### Race Condition
- When two threads or processes interact with a shared resource in an unintended way, overwriting the actions of the other or performing actions out of order.

### Deadlock
- When two threads are waiting on a lock that the other has.

### Scope
- Refers to variables that can be accessed at a given location in code.

### continue
- Keyword for immediately jumping to the next iteration of a loop.

### Future
- A read only placeholder set by a promise.

### Promise
- Sets the value of a future when the result is known.

### Delay / Deferred / Async / Yield
- A placeholder is returned while the result is calculated concurrently.
- Evaluating the placeholder will block until the result is calculated.

### Sentinal Value
- When certain values in a variables domain are used as a flag.

### Thread
- An independant unit of execution within a process.
- Threads in a process share the heap but have their own stack.

### Process
- Program code executed by one or more threads.

### Heap
- Portion of program memory where global variables are stored and dynamic variables are allocated.

### Stack
- Portion of program memory that tracks local variables.

### Unit Test
- The smallest level of test which ideally tests one requirement of one part of the software.

### System Test
- A test that verifies software behavior across multiple componants.

### Polymorphism
- Functionality that allows one type to act as multiple different types, or multiple different types to share the same interface.
- Can be achieved with:
-- Interface
-- Generics/Templates
-- Subtyping

### Inheritance
- Mechanism that allows one type to define the base of another.

### Override
- Mechanism that allows a derived class to provide its own implementation for methods in its base class.

### Virtual
- Derived classes can override members with this modifier.

### Base Class / Parent Class / Super Class
- The class that another class derives from.

### Derived Class
- The class that inherits from another class.

### Class
- A template for creating objects.
- Object Oriented Programming

### Object
- A realization of a class.
- Object Oriented Programming

### Instance
- Realization of a primitive or object in memory.

### Duck Typing
- Objects can be used in any context so long as they implement members being used.
- If it looks like a duck and quacks like a duck, it must be a duck.

### Strongly Typed
- Usually implies a language with type safety, memory safety, static type checking.

### Weakly Typed
- Usually implies a langauge with no type safety, no memory safety, dynamic type checking.

### Statically Typed Language
- Object types are checked at compile time.

### Dynamically Typed Language
- Object types are checked at runtime.

### Map
- Provided a function f(x) and input collection, return a collection with a one-to-one relation of f(x) for every x in input.

### Reduce
- Provided a function f(x, c) and input collection, combine each element with the result of the previous element.

### Functional Programming
- A programming paradigm where side effects are avoided.
- Clojure, Lisp, F#

### Imperative Programming
- A programming paradigm where statements change the programs state.

### Procedural programming
- A programming paradigm where statements are grouped into procedures.
- A subtype of imperative programming.

### Declaritive programming
- Programming paradigm where the result is defined by constraints instead of instructions.
- SQL, Prolog

### Object Oriented Programming
- Programming paradigm where functionality is defined by procedures and state, defining new types.

### TCP
- Send messages with a guarantee of delivery and order.

### UDP
- Protocol for sending messages without a guarantee of delivery or order.

### Socket
- Similar to a file descriptor, defined by an ip and a port.

### Garbage Collection
- The process by which the memory for variables that go out of scope is reclaimed.

### Wrapper Class
- A class that hides the functionality of another component and provides its own interface.

### Primitive
- A built in type provided by the programming language.

### Default Constructor
- The default constructor is called when an object is initialized and no constructor was explicitly defined.

### Conversion Constructor
- A conversion constructor is any constructor that takes one argument, it initializes an instance of the constructors class from the provided type.

### Multiple Inheritance
- A feature in some langauges that allows you to derive a class from two or more base classes.
- Can be confusing if both base classes expose members with the same identifier.
- The order that super classes are called is implementation dependant and can add further confusion.

