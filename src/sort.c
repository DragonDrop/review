#include <stddef.h>
#include <stdio.h>

int partition (int array[], int length) {
    int pivot = array[length - 1];
    int i = 0;
    int j = 0;
    int temp;
    for (; j < length - 1; j++) {
        if (array[j] < pivot) {
            // Swap i with j
            temp = array[j];
            array[j] = array[i];
            array[i] = temp;
            i++;
        }
    }
    // Swap pivot with i.
    temp = array[length-1];
    array[length-1] = array[i];
    array[i] = temp;
    return i;
}
void quickSort (int array[], int length) {
    /*Divide and conquer, pick pivot partition around pivot.*/
    // 1. Base case.
    if (array == NULL || length <= 1) return;
    // 2. Select Pivot, partition array.
    int partitionIndex = partition(array, length);
    // Recur on sub arrays.
    quickSort(array, partitionIndex);
    quickSort(array + partitionIndex + 1, length - partitionIndex - 1);
}

void merge (int array[], int middle, int length) {
    int i = 0;
    int j = 0;
    int k = 0;
    int lLength = middle;
    int rLength = length - middle;
    int lArray[lLength];
    int rArray[rLength];
    for (i = 0; i < lLength; i++) {
        lArray[i] = array[i];
    }
    for (j = 0; j < rLength; j++) {
        rArray[j] = array[lLength+j];
    }
    i = 0; j = 0; k = 0;
    while (i < lLength && j < rLength) {
        if (lArray[i] < rArray[j]) {
            array[k] = lArray[i];
            i++;
        }
        else {
            array[k] = rArray[j];
            j++;
        }
        k++;
    }
    while (i < lLength) {
        array[k] = lArray[i];
        i++;
        k++;
    }
    while (j < rLength) {
        array[k] = rArray[j];
        j++;
        k++;
    }
}
void mergeSort (int array[], int length) {
    if (array == NULL || length <= 1) return;
    int middle = length / 2;
    mergeSort(array, middle);
    mergeSort(array + middle, length - middle);
    merge(array, middle, length);
}

void selectionSort (int array[], int length) {
    if (array == NULL) return;
    for (int i = 0; i < length; i++) {
        int minIndex = i;
        for (int j = i+1; j < length; j++) {
            if (array[j] < array[minIndex]) {
                minIndex = j;
            }
        }
        int temp = array[i];
        array[i] = array[minIndex];
        array[minIndex] = temp;
    }
}

void insertionSort (int array[], int length) {
    for (int i = 0; i < length; i++) {
        for (int j = i; j > 1; j++) {
            if (array[j] > array[j-1]) {
                int temp = array[j];
                array[j] = array[j-1];
                array[j-1] = temp;
            }
        }
    }
}
