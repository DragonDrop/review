#include "assertion.h"

int findMostFrequent(int numbers[], int count);
int binarySearch(int array[], int length, int value);
int fibRecursive(int position);
int fibIterative(int position);
int parenCheck(char* input);

void testBinarySearch () {
    int input0[] = {0, 2, 5, 13, 14, 20, 100};
    for (int i = 0; i < 7; i++) {
        int actual = binarySearch(input0, 7, input0[i]);
        assertIntegersEqual(actual, i);
    }
}

void testFibRecursive () {
    int input0[] = {0, 1, 1, 2, 3, 5, 8, 13, 21};
    for (int i = 0; i < 9; i++) {
        int result = fibRecursive(i);
        int actual = input0[i];
        assertIntegersEqual(result, actual);
    }
}

void testFibIterative () {
    int input0[] = {0, 1, 1, 2, 3, 5, 8, 13, 21};
    for (int i = 0; i < 9; i++) {
        int result = fibIterative(i);
        int actual = input0[i];
        assertIntegersEqual(result, actual);
    }
}

void testParenCheck () {
    char input0[] = "";
    int result = parenCheck(input0);
    assertIntegersEqual(1, result);

    char input1[] = "(";
    result = parenCheck(input1);
    assertIntegersEqual(0, result);

    char input2[] = ")";
    result = parenCheck(input2);
    assertIntegersEqual(0, result);

    char input3[] = ")(";
    result = parenCheck(input3);
    assertIntegersEqual(0, result);

    char input4[] = "()";
    result = parenCheck(input4);
    assertIntegersEqual(1, result);

    char input5[] = "()()";
    result = parenCheck(input5);
    assertIntegersEqual(1, result);

    char input6[] = "(())";
    result = parenCheck(input6);
    assertIntegersEqual(1, result);
}

void testFindMostFrequent() {
    int input1[] = {1, 2, 3};
    int result = findMostFrequent(input1, 3);
    assertIntegersEqual(result, 1);

    int input2[] = {1, 2, 2};
    result = findMostFrequent(input2, 3);
    assertIntegersEqual(result, 2);

    int input3[] = {2, 2, 2};
    result = findMostFrequent(input3, 3);
    assertIntegersEqual(result, 2);

    int input4[] = {2, 2, 2, 3, 3, 3, 5, 5, 5, 5};
    result = findMostFrequent(input4, 10);
    assertIntegersEqual(result, 5);

    int input5[] = {5, 5, 2, 2, 2, 5, 5};
    result = findMostFrequent(input5, 7);
    assertIntegersEqual(result, 5);
}

void generalTest() {
    testFindMostFrequent();
    testBinarySearch();
    testFibRecursive();
    testFibIterative();
    testParenCheck();
}
