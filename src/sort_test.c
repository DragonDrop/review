#include "assertion.h"

void quickSort (int array[], int length);
void mergeSort (int array[], int length);
void selectionSort (int array[], int length);
void insertionSort (int array[], int length);

void testQuickSort() {
    int input0[] = {0};
    quickSort(input0, 0);

    int input1[] = {5};
    quickSort(input1, 1);
    assertIntegersEqual(input1[0], 5);

    int input2[] = {5, 1};
    quickSort(input2, 2);
    assertIntegersEqual(input2[0], 1);
    assertIntegersEqual(input2[1], 5);

    int input3[] = {5, 4, 3, 2, 1, 0};
    quickSort(input3, 6);
    assertIntegersEqual(input3[0], 0);
    assertIntegersEqual(input3[1], 1);
    assertIntegersEqual(input3[2], 2);
    assertIntegersEqual(input3[3], 3);
    assertIntegersEqual(input3[4], 4);
    assertIntegersEqual(input3[5], 5);
}

void testMergeSort() {
    int input0[] = {0};
    mergeSort(input0, 0);

    int input1[] = {5};
    mergeSort(input1, 1);
    assertIntegersEqual(input1[0], 5);

    int input2[] = {5, 1};
    mergeSort(input2, 2);
    assertIntegersEqual(input2[0], 1);
    assertIntegersEqual(input2[1], 5);

    int input3[] = {5, 4, 3, 2, 1, 0};
    mergeSort(input3, 6);

    assertIntegersEqual(input3[0], 0);
    assertIntegersEqual(input3[1], 1);
    assertIntegersEqual(input3[2], 2);
    assertIntegersEqual(input3[3], 3);
    assertIntegersEqual(input3[4], 4);
    assertIntegersEqual(input3[5], 5);
}

void testSelectionSort() {
    int input0[] = {0};
    mergeSort(input0, 0);

    int input1[] = {5};
    mergeSort(input1, 1);
    assertIntegersEqual(input1[0], 5);

    int input2[] = {5, 1};
    mergeSort(input2, 2);
    assertIntegersEqual(input2[0], 1);
    assertIntegersEqual(input2[1], 5);

    int input3[] = {5, 4, 3, 2, 1, 0};
    mergeSort(input3, 6);

    assertIntegersEqual(input3[0], 0);
    assertIntegersEqual(input3[1], 1);
    assertIntegersEqual(input3[2], 2);
    assertIntegersEqual(input3[3], 3);
    assertIntegersEqual(input3[4], 4);
    assertIntegersEqual(input3[5], 5);
}

void testInsertionSort () {
    int input0[] = {0};
    mergeSort(input0, 0);

    int input1[] = {5};
    mergeSort(input1, 1);
    assertIntegersEqual(input1[0], 5);

    int input2[] = {5, 1};
    mergeSort(input2, 2);
    assertIntegersEqual(input2[0], 1);
    assertIntegersEqual(input2[1], 5);

    int input3[] = {5, 4, 3, 2, 1, 0};
    mergeSort(input3, 6);

    assertIntegersEqual(input3[0], 0);
    assertIntegersEqual(input3[1], 1);
    assertIntegersEqual(input3[2], 2);
    assertIntegersEqual(input3[3], 3);
    assertIntegersEqual(input3[4], 4);
    assertIntegersEqual(input3[5], 5);
}


void sortTest() {
    testQuickSort();
    testMergeSort();
    testSelectionSort();
    testInsertionSort();
}

