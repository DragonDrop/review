#include <stddef.h>
#include <stdio.h>

void quickSort(int numbers[], int length);

int binarySearch(int array[], int length, int value) {
    if (array == NULL || length < 1) return -1;
    int middle = length / 2;
    if (value == array[middle]) {
        return middle;
    }
    else if (value > array[middle]) {
        return middle + 1 + binarySearch(array + middle+1, length - middle - 1, value);
    } else {
        return binarySearch(array, middle, value);
    }
}

int fibRecursive(int position) {
    if (position < 2) return position;
    return fibRecursive(position-2) + fibRecursive(position-1);
}

int fibIterative(int position) {
    if (position <= 0) return 0;
    int last = 0;
    int next = 1;
    for (int i = 1; i < position; i++) {
        int temp = next;
        next = last + next;
        last = temp;
    }
    return next;
}

int parenCheck(char* input) {
    int level = 0;
    while (*input) {
        if (*input == '(') {
            level++;
        }
        else if (*input == ')') {
            level--;
        }
        if (level < 0) {
            return 0;
        }
        input++;
    }
    return level == 0;
}

int findMostFrequent(int numbers[], int length) {
    if (numbers == NULL || length == 0) return -1;
    quickSort(numbers, length);

    int mostNum = 0;
    int mostCount = 0; // A value of zero implies first pass.
    int lastNum = 0;
    int lastCount = 0;
    for (int i = 0; i < length; i++) {
        int number = numbers[i];
        if (mostCount == 0) {
            // First number, do setup.
            mostNum = lastNum = number;
            mostCount = lastCount = 1;
        }
        else if (number == lastNum) {
            lastCount++;
            if (lastCount > mostCount) {
                mostNum = lastNum;
                mostCount = lastCount;
            }
        }
        else {
            lastNum = number;
            lastCount = 1;
        }
    }
    return mostNum;
}

