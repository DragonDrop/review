# REVIEW

Collection of questions, terminology, and code samples that are commonly found in programming interviews.

Questions and terms are located under doc/documentation.md, code samples are located under src.

TIPS:

- Write terms out in your own words so they're easier to absorb under your own vocabulary. Then you can practice them that way, or convert them into flash cards.
- When practicing programming problems, delete the example implementation and write your own. In an actual interview you won't have a compiler, practice running unit tests by hand, keeping track of the program state. Once you are confident you have the correct answer, run the unit tests as final validation.

## Prerequisites
Build
: gcc https://gcc.gnu.org/install/

Coverage Report
: gcovr https://github.com/gcovr/gcovr

Documentation
: pandoc https://pandoc.org/installing.html

## Usage
Build
: `make build`

Test
: `make test`

Coverage Report
: `make coverage`

Documentation
: `make docs`

## Authors

- Paige Xura
